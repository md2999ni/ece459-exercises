use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N_SENDERS: i32 = 2;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    let mut children = Vec::new();

    // Create N_SENDERS child thread, the main thread will be the "listener"
    for i in 0..N_SENDERS {
        // We need to clone the transmission channel
        let thread_local_tx = mpsc::Sender::clone(&tx);

        // We need to create the child thread
        let child = thread::spawn(move || {
            let message = format!("Hello from thread {}", i);

            // Send a message through the channel.
            thread_local_tx.send(message).unwrap();

            thread::sleep(Duration::from_millis(1000));
        });
        
        // Add to the vector of children
        children.push(child);
    }

    // Join all children threads
    for child in children {
        child.join().unwrap();
    }

    // [IMPORTANT]
    // The following call wil block if there are still senders 'on the line'. Therefore we have to
    // explicitly drop 'tx' on the main thread so we don't block while waiting for messages from ourself!
    drop(tx);

    // Output the messages received by the main thread across the channel.
    for received in rx {
        println!("Got: {}", received);
    }
}
