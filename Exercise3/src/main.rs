use std::thread;
use std::time::Duration;


static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = Vec::new();

    for i in 0..N {
        let t = thread::spawn(move || {
            println!("created: {}", i);
            thread::sleep(Duration::from_millis(500));
        });

        children.push(t);
    }

    for (i, t) in children.into_iter().enumerate() {
        println!("joining: {}", i);
        t.join().unwrap();
    }
}
