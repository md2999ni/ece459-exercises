// You should implement the following function
use std::cmp::Ordering;


fn fibonacci_number(n: u32) -> u32 {
    return match n.cmp(&1) {
        Ordering::Less | Ordering::Equal => 1,
        _ => fibonacci_number(n - 1) + fibonacci_number(n - 2),
    }
}


fn main() {
    println!("{}", fibonacci_number(10));
}
